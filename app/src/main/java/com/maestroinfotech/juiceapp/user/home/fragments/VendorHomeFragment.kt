package com.maestroinfotech.juiceapp.user.home.fragments

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.FragmentVendorHomeBinding
import com.maestroinfotech.juiceapp.user.home.UserHomeActivity
import com.maestroinfotech.juiceapp.vendor.home.fragments.adapter.JuiceAdapter
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.UserFragmentAdapterEventListener
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.Restaurant
import com.maestroinfotech.juiceapp.vendor.home.VendorHomeActivity
import com.maestroinfotech.juiceapp.vendor.home.fragments.AddFruitJuiceFragment
import com.maestroinfotech.juiceapp.vendor.home.model.JuiceRegisterResponse


class VendorHomeFragment : Fragment() {

    lateinit var binding:FragmentVendorHomeBinding


    companion object {


        var userFragmentAdapterEventListener: UserFragmentAdapterEventListener?=null
        

        val restaurant = listOf<Restaurant>(
            Restaurant(R.drawable.juice_res_seven, "Juicofest"),

            Restaurant(R.drawable.juice_res_four, "Mangofest"),
            Restaurant(R.drawable.juice_res_six, "Grapeofest"),
            Restaurant(R.drawable.juice_res_two, "AppleBar"),
            Restaurant(R.drawable.juice_res_three, "PinnyTown"),
            Restaurant(R.drawable.juice_res_five, "Mangofest"),
            Restaurant(R.drawable.juice_res_one, "Mangofest"),
            )


    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentVendorHomeBinding.inflate(layoutInflater)

            VendorHomeActivity.binding!!.appBarMain.appBarLayout.visibility = View.VISIBLE


        getAllJuices()





    binding.addFruitJuiceButton.setOnClickListener {



        VendorHomeActivity.navigateTo(
            requireActivity().supportFragmentManager,
            AddFruitJuiceFragment()

        )

    }




     VendorHomeFragment.userFragmentAdapterEventListener= object:UserFragmentAdapterEventListener{
            override fun navigateToVendorDetail() {
                UserHomeActivity.navigateTo(requireActivity().supportFragmentManager,VendorDetailFragment())
            }

         override fun getPickedImageProduct(uri: Uri) {

         }

     }


        return binding.root
    }




    fun getAllJuices(){

        Constant.showDialog(requireActivity(),"Get all juices..")

        AndroidNetworking.post("https://ruparnatechnology.com/juice/appservice/process.php?action=show_all_juice")
            .addBodyParameter("user_id", Constant.getUserData(requireContext()).id)
            .build().getAsObjectList(JuiceRegisterResponse::class.java,object :ParsedRequestListener<List<JuiceRegisterResponse>>{
                override fun onResponse(response: List<JuiceRegisterResponse>?) {

                    if (response!=null) {
                        Constant.dismiss()
                        if (response!!.size > 0)
                            binding.productList.apply {
                                layoutManager = GridLayoutManager(context, 2)
                                adapter = JuiceAdapter(response!!)

                            }



                    }else {
                        Constant.dismiss()

                        Constant.showToast(requireContext(),"Servor error occured")
                    }
                }

                override fun onError(anError: ANError?) {
                    Constant.dismiss()
                }


            })



    }





}