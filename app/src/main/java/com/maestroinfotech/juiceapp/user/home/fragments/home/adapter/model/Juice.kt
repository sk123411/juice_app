package com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model

data class Juice(val prImage:Int?, val prTitle:String?)
