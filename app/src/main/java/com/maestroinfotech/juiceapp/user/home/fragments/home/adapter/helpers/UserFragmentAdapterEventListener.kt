package com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers

import android.net.Uri

interface UserFragmentAdapterEventListener {


    fun navigateToVendorDetail()
    fun getPickedImageProduct(uri: Uri)

}