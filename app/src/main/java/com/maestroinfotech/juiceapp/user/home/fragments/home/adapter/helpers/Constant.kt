package com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.text.InputType
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.UserRegisterResponse
import io.paperdb.Paper
import java.net.URL

class Constant {

    companion object {
        val REGISTERED_SHOP: String?="registered_shop"
        val NUMBER: String?="number"
        var dialog:ProgressDialog? = null
        val userDetails:String? ="user_details"


        fun showDialog(appCompatActivity: FragmentActivity,message:String){

            dialog = ProgressDialog(appCompatActivity)
            dialog!!.setMessage(message)
            dialog!!.show()


        }

        fun dismiss(){

            dialog!!.dismiss()


        }

         inline fun< reified T :Class<T>> request(url:String?, requesttype: REQUESTTYPE,
                                               data:HashMap<String,String>):T{
            var datObject:T? = null
            if (requesttype==REQUESTTYPE.GET){

                AndroidNetworking.get(url)
                    .build().getAsObject(T::class.java, object:ParsedRequestListener<T>{
                        override fun onResponse(response: T) {


                            Log.d("TESTTTTTTTT", "TESTTTTTTTTT")



                        }

                        override fun onError(anError: ANError?) {

                        }


                    })



            }else if (requesttype==REQUESTTYPE.POST){





            }
             return datObject!!

        }


        fun showToast(context: Context,message: String){
            Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
        }


        fun getUserData(context: Context):UserRegisterResponse{
            Paper.init(context)
            return Paper.book().read<UserRegisterResponse>(Constant.userDetails)
        }





    }



    enum class TEXTTYPE{
        EMAIL,TEXT,NUMBER

    }


    enum class REQUESTTYPE{
        GET,POST,UPLOAD

    }

}