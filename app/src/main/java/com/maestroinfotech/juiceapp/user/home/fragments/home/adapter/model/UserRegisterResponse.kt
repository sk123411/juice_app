package com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model


import com.google.gson.annotations.SerializedName

data class UserRegisterResponse(
    @SerializedName("confirm_password")
    val confirmPassword: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("password")
    val password: String?,
    @SerializedName("result")
    val result: String?,
    @SerializedName("strtotime")
    val strtotime: String?,
    @SerializedName("user_type")
    val userType: String?,
    @SerializedName("message")
    val message:String?,
    @SerializedName("otp")
    val otp:String?


)