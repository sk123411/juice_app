package com.maestroinfotech.juiceapp.user.home.fragments.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestroinfotech.juiceapp.databinding.ProductItemBinding
import com.maestroinfotech.juiceapp.user.home.fragments.VendorHomeFragment
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.Restaurant

class RestaurantAdapter(val restaurants:List<Restaurant>) : RecyclerView.Adapter<RestaurantAdapter.MyViewHolder>() {
    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {
        val binding = ProductItemBinding.bind(view)



        fun bindata(restaurant: Restaurant) {

            binding.productTitle.setText(restaurant.prTitle)
            binding.productImage.setImageResource(restaurant.prImage!!)

            binding.root.setOnClickListener {

                VendorHomeFragment.userFragmentAdapterEventListener!!.navigateToVendorDetail()
            }

        }


         }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(ProductItemBinding.inflate(LayoutInflater.from(parent.context)).root)


    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.bindata(restaurants.get(position))
    }

    override fun getItemCount(): Int {
        return restaurants.size
    }
}