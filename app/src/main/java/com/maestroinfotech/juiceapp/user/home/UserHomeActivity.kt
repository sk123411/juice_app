package com.maestroinfotech.juiceapp.user.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.navigation.NavigationView
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.ActivityHomeBinding
import com.maestroinfotech.juiceapp.user.home.fragments.VendorHomeFragment

class UserHomeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbarMain.toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24)
        actionbar?.setDisplayHomeAsUpEnabled(true)




        binding.appBarMain.toolbarMain.toolbar.setNavigationOnClickListener {

            binding.drawerLayout.openDrawer(GravityCompat.START)

        }




       binding!!.navView.setNavigationItemSelectedListener(
            object : NavigationView.OnNavigationItemSelectedListener {
                override fun onNavigationItemSelected(item: MenuItem): Boolean {

                    when(item.itemId){


                        R.id.nav_edit_product -> {

                        binding!!.drawerLayout.closeDrawers()


                        }


                        R.id.nav_total_revenue -> {

                        binding!!.drawerLayout.closeDrawers()


                        }


                        R.id.nav_feedback -> {

                         binding!!.drawerLayout.closeDrawers()


                        }


                        R.id.nav_changePasword -> {

                          binding!!.drawerLayout.closeDrawers()


                        }


                        R.id.nav_offers -> {

                         binding!!.drawerLayout.closeDrawers()

                        }


                        R.id.nav_logout -> {

                         binding!!.drawerLayout.closeDrawers()


                        }


                        else -> {
                          binding!!.drawerLayout.closeDrawers()

                        }


                    }


                    return false
                }


            })


        val fragmentManager = supportFragmentManager.beginTransaction()
            .add(R.id.container, VendorHomeFragment()).commit()



    }

    companion object {

        lateinit var binding :ActivityHomeBinding
        fun navigateTo(supportFragmentManager: FragmentManager, fragment: Fragment) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment).addToBackStack(null).commit()
        }
    }
}