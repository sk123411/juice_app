package com.maestroinfotech.juiceapp.user.home.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.FragmentVendorDetailBinding
import com.maestroinfotech.juiceapp.vendor.home.fragments.adapter.JuiceAdapter
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.SliderAdapter
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.Juice
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.Slider
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations


class VendorDetailFragment : Fragment() {

    lateinit var binding:FragmentVendorDetailBinding

    companion object {



        val product = listOf<Juice>(
            Juice(R.drawable.juice_res_one, "Banana"),

            Juice(R.drawable.juice_res_two, "Mango"),

            Juice(R.drawable.juice_res_three, "Grapes"),

            Juice(R.drawable.juice_res_six, "Pinnapple"),

            Juice(R.drawable.juice_res_seven, "Strawberry"),
            )

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

            binding = FragmentVendorDetailBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment


        val  sliderAdapter = SliderAdapter(requireContext())
        for (data in product){
            sliderAdapter.addItem(
                Slider(
                    data.prTitle,
                    data.prImage
                )
            )

        }
        binding.vendorImageSlider.scrollTimeInMillis = 3000
        binding.vendorImageSlider.isAutoCycle = true
        binding.vendorImageSlider.setSliderAdapter(sliderAdapter)
        binding.vendorImageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
        binding.vendorImageSlider.setSliderTransformAnimation(SliderAnimations.ANTICLOCKSPINTRANSFORMATION);
        binding.vendorImageSlider.startAutoCycle();








        return binding.root



    }


}