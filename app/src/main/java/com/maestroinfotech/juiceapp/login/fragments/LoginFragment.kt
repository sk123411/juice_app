package com.maestroinfotech.juiceapp.login.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.LayoutSignInBinding
import com.maestroinfotech.juiceapp.user.home.UserHomeActivity
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.vendor.home.VendorHomeActivity
import org.json.JSONObject
import kotlin.math.sign


class LoginFragment : Fragment() {

    lateinit var binding:LayoutSignInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = LayoutSignInBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment


        binding.fpButton.setOnClickListener {

            navigateTo(requireActivity().supportFragmentManager, ForgotPasswordFragment())

        }



        binding.signInButton.setOnClickListener {

            Constant.showDialog(requireActivity(),"Please wait...")

            if (binding.editNumber.text.toString().isEmpty()){
                Toast.makeText(context, "Please enter your number", Toast.LENGTH_LONG).show()
            return@setOnClickListener
            }
            if (binding.editPassword.text.toString().isEmpty()){
                Toast.makeText(context, "Please enter your pasword", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            signIN()


        }


        binding.signUpButton.setOnClickListener {

            requireActivity().supportFragmentManager.beginTransaction()
                .add(R.id.container, RegisterFragment()).addToBackStack(null).commit()
        }

        return binding.root
    }

    fun signIN(){

        AndroidNetworking.post("https://ruparnatechnology.com/juice/appservice/process.php?action=login")
            .addBodyParameter("phone", binding.editNumber.text.toString())
            .addBodyParameter("password", binding.editPassword.text.toString())
            .build().getAsJSONObject(object:JSONObjectRequestListener{
                override fun onResponse(response: JSONObject?) {

                    Constant.dismiss()
                    if (response!!.getString("result").contains("sucessfully")){


                            startActivity(Intent(context,VendorHomeActivity::class.java ))


                    }

                }

                override fun onError(anError: ANError?) {

                    Constant.dismiss()

                }

            })

    }


    companion object {

        fun navigateTo(supportFragmentManager: FragmentManager,fragment: Fragment) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment).addToBackStack(null).commit()
        }
    }

}

