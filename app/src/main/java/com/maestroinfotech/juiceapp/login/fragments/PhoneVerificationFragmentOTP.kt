package com.maestroinfotech.juiceapp.login.fragments

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.maestroinfotech.juiceapp.databinding.LayoutPhnVerificationOtpBinding
import com.maestroinfotech.juiceapp.user.home.fragments.VendorHomeFragment
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.UserRegisterResponse
import com.maestroinfotech.juiceapp.vendor.home.VendorHomeActivity
import io.paperdb.Paper



class PhoneVerificationFragmentOTP : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var binding: LayoutPhnVerificationOtpBinding
    var otpText:String?=""
    var otpOne:String?=""
    var otpTwo:String?=""
    var otpThree:String?=""
    var otpFour:String?=""
    var otpFive:String?=""
    var otpSix:String?=""




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = LayoutPhnVerificationOtpBinding.inflate(layoutInflater)

        binding.backButton.setOnClickListener {

            requireActivity().supportFragmentManager.popBackStack()
        }




        binding.otpEditOne.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.length==1){

                    binding.otpEditTwo.requestFocus()
                    otpOne = s.toString()

                }

            }

            override fun afterTextChanged(s: Editable?) {

            }


        })

        binding.otpEditTwo.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.length==1){

                    binding.otpEditThree.requestFocus()
                   otpTwo = s.toString()

                }

            }

            override fun afterTextChanged(s: Editable?) {

            }


        })

        binding.otpEditThree.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.length==1){

                    binding.otpEditFour.requestFocus()
                    otpThree = s.toString()

                }

            }

            override fun afterTextChanged(s: Editable?) {

            }


        })

        binding.otpEditFour.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.length==1){

                    binding.otpEditFive.requestFocus()
                    otpFour = s.toString()

                }

            }

            override fun afterTextChanged(s: Editable?) {

            }


        })

        binding.otpEditFive.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.length==1){

                    binding.otpEditSix.requestFocus()
                    otpFive = s.toString()

                }

            }

            override fun afterTextChanged(s: Editable?) {

            }


        })

        binding.otpEditSix.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.length==1){

                    binding.otpEditSix.requestFocus()
                    otpSix = s.toString()

                }

            }

            override fun afterTextChanged(s: Editable?) {

            }


        })





        binding.verifyButton.setOnClickListener {

            Paper.init(context)
            val number = Paper.book().read<String>(Constant.NUMBER)
            otpText = otpOne+otpTwo+otpThree+otpFour+otpFive+otpSix

            if (otpText!!.length<5){

                Toast.makeText(context, "Please enter otp", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }


            otpText = otpOne+otpTwo+otpThree+otpFour+otpFive+otpSix
            Log.d("OTP", "::"+otpText)
            verifyOTP(number)


        }




        binding.resendButton.setOnClickListener {

            Toast.makeText(context, "Otp resend successfully",Toast.LENGTH_SHORT).show()
        }




        // Inflate the layout for this fragment
        return binding.root
    }

    private fun verifyOTP(number:String) {
        Constant.showDialog(requireActivity(), "Please wait...")
        Paper.init(context)
        val signupResponse = Paper.book().read<UserRegisterResponse>(Constant.userDetails)


        AndroidNetworking.post("https://ruparnatechnology.com/juice/appservice/process.php?action=verify_otp")
            .addBodyParameter("phone",number )
            .addBodyParameter("otp", otpText)
            .addBodyParameter("user_id", signupResponse.id)
            .build().getAsObject(UserRegisterResponse::class.java, object:ParsedRequestListener<UserRegisterResponse>{
                override fun onResponse(response: UserRegisterResponse?) {

                    Constant.dismiss()

                    if (response!!.result!!.contains("Successfully")){

                        Constant.dismiss()

                        startActivity(Intent(context,VendorHomeActivity::class.java))


                    }else {

                        Toast.makeText(context, "OTP not matched",Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onError(anError: ANError?) {

                    Constant.dismiss()

                }


            })


    }


}