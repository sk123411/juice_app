package com.maestroinfotech.juiceapp.login.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maestroinfotech.juiceapp.databinding.LayoutForgotPasswordBinding


class ForgotPasswordFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var binding: LayoutForgotPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = LayoutForgotPasswordBinding.inflate(layoutInflater)

        binding.backButton.setOnClickListener {

            requireActivity().supportFragmentManager.popBackStack()
        }

        binding.sendButton.setOnClickListener {

            LoginFragment.navigateTo(
                requireActivity().supportFragmentManager,
                ResetPasswordFragment()
            )
        }




        // Inflate the layout for this fragment
        return binding.root
    }

    companion object {

    }
}