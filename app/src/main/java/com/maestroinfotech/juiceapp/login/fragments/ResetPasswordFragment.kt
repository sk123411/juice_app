package com.maestroinfotech.juiceapp.login.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maestroinfotech.juiceapp.databinding.LayoutResetPasswordBinding


/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ResetPasswordFragment : Fragment() {

    lateinit var binding: LayoutResetPasswordBinding



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = LayoutResetPasswordBinding.inflate(layoutInflater)

        // Inflate the layout for this fragment
        binding.backButton.setOnClickListener {

            requireActivity().supportFragmentManager.popBackStack()
        }






        return binding.root
    }

    companion object {

    }
}