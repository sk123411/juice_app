package com.maestroinfotech.juiceapp.login.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.maestroinfotech.juiceapp.databinding.LayoutSignUpBinding
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.UserRegisterResponse
import io.paperdb.Paper

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var binding: LayoutSignUpBinding
    var type:String? = "1"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = LayoutSignUpBinding.inflate(layoutInflater)









        binding.signUpButton.setOnClickListener {
            Constant.showDialog(requireActivity(),"Please wait")


            if (binding.editName.text.toString().isEmpty() ){

                Toast.makeText(context, "Please enter your name", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (binding.editEmail.text.toString().isEmpty() ||!binding.editEmail.text.toString().contains("@") ){

                Toast.makeText(context, "Please enter valid email", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if (binding.editPassword.text.toString().isEmpty() ){

                Toast.makeText(context, "Please enter your password", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (!binding.editPassword.text.toString().equals(binding.editCnfPassword.text.toString()) ){

                Toast.makeText(context, "Please check password and condirm password", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            register()

        }






        return binding.root





    }








    fun register(){

        AndroidNetworking.post("https://ruparnatechnology.com/juice/appservice/process.php?action=signup")
            .addBodyParameter("name", binding.editName.text.toString())
            .addBodyParameter("email", binding.editEmail.text.toString())
            .addBodyParameter("password",binding.editPassword.text.toString())
            .addBodyParameter("type", type)
            .build().getAsObject(UserRegisterResponse::class.java, object:ParsedRequestListener<UserRegisterResponse>{
                override fun onResponse(response: UserRegisterResponse?) {


                    Constant.dismiss()

                        Paper.init(context)
                        Paper.book().write(Constant.userDetails,response)
                        Toast.makeText(requireActivity(), "Sign up success! You can login",Toast.LENGTH_LONG).show()
                            LoginFragment.navigateTo(requireActivity().supportFragmentManager,
                            PhoneVerificationFragment())





                }

                override fun onError(anError: ANError?) {
                    Log.d("RESSSSSSSSS", ":::"+anError.toString())

                    Constant.dismiss()


                }


            })

    }





}