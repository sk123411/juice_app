package com.maestroinfotech.juiceapp.login.fragments.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.login.fragments.LoginFragment

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        val fragmentManager = supportFragmentManager.beginTransaction()
            .add(R.id.container, LoginFragment()).commit()

    }


}