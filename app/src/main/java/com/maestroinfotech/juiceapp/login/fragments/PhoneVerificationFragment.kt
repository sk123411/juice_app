package com.maestroinfotech.juiceapp.login.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.maestroinfotech.juiceapp.databinding.LayoutPhnVerificationBinding
import com.maestroinfotech.juiceapp.user.home.UserHomeActivity
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.UserRegisterResponse
import com.maestroinfotech.juiceapp.vendor.home.VendorHomeActivity
import io.paperdb.Paper


class PhoneVerificationFragment : Fragment() {

    lateinit var binding: LayoutPhnVerificationBinding
    var signUpResponse:UserRegisterResponse? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = LayoutPhnVerificationBinding.inflate(layoutInflater)

        binding.backButton.setOnClickListener {

            requireActivity().supportFragmentManager.popBackStack()
        }

        binding.confirmButton.setOnClickListener {

            if (binding.editNumber.text.toString().equals("")||binding.editNumber.text.toString().length !=10){
                Toast.makeText(context, "Please check number is of 10 length and correct",Toast.LENGTH_LONG).show()
                return@setOnClickListener

            }

            generateOTP(binding.editNumber.text.toString())


        }




        return binding.root
    }


    fun generateOTP(number:String){
        Constant.showDialog(requireActivity(), "Please wait...")

        Paper.init(requireContext())
        Paper.book().write(Constant.NUMBER, number)
        signUpResponse = Paper.book().read<UserRegisterResponse>(Constant.userDetails)




        AndroidNetworking.post("https://ruparnatechnology.com/juice/appservice/process.php?action=generate_otp")
            .addBodyParameter("user_id", signUpResponse!!.id)
            .addBodyParameter("phone", binding.editNumber.text.toString())
            .build().getAsObject(UserRegisterResponse::class.java, object:ParsedRequestListener<UserRegisterResponse>{
                override fun onResponse(response: UserRegisterResponse?) {

                    if (response!!.message!!.contains("Successfull")){
                        Constant.dismiss()

                        Paper.init(requireContext())
                        Paper.book().write(Constant.userDetails, response)
                        LoginFragment.navigateTo(requireActivity().supportFragmentManager,
                            PhoneVerificationFragmentOTP())

                    }



                }

                override fun onError(anError: ANError?) {
                    Constant.dismiss()

                }

            })


    }

}