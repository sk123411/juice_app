package com.maestroinfotech.juiceapp.vendor.home.model


import com.google.gson.annotations.SerializedName

data class ShopRegisterResponse(
    @SerializedName("address")
    val address: String?,
    @SerializedName("area_pincode")
    val areaPincode: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("owner_name")
    val ownerName: String?,
    @SerializedName("result")
    val result: String?,
    @SerializedName("shop_name")
    val shopName: String?,
    @SerializedName("shop_registration_bbmp")
    val shopRegistrationBbmp: String?,
    @SerializedName("strtotime")
    val strtotime: String?
)