package com.maestroinfotech.juiceapp.vendor.home.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.LayoutNewOnlineShopBinding
import com.maestroinfotech.juiceapp.databinding.LayoutShopRegistrationBinding
import com.maestroinfotech.juiceapp.vendor.home.VendorHomeActivity
import com.maestroinfotech.juiceapp.vendor.home.fragments.adapter.SelectJuiceTypeAdapter
import com.maestroinfotech.juiceapp.vendor.home.fragments.adapter.TempJuice


class SelectJuiceTypeFragment : Fragment() {

    lateinit var binding:LayoutNewOnlineShopBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        binding = LayoutNewOnlineShopBinding.inflate(layoutInflater)




        binding.shopItemList.apply {

            layoutManager = LinearLayoutManager(context)
            adapter = SelectJuiceTypeAdapter(listOf(
                TempJuice(R.drawable.banana, "Rs 10"),
                TempJuice(R.drawable.pineaaple, "Rs 50"),

                TempJuice(R.drawable.strawberry, "Rs 50"),

                TempJuice(R.drawable.grapes, "Rs 50"),
                TempJuice(R.drawable.mango, "Rs 50"),
                TempJuice(R.drawable.juice_res_one, "Rs 50"),
            )

            )

        }


        binding.addMoreButton.setOnClickListener {


            VendorHomeActivity.navigateTo(requireFragmentManager(),
            ShopRegisterSplashFragment())



        }






        return binding.root
    }


}