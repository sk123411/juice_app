package com.maestroinfotech.juiceapp.vendor.home.fragments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.ProductItemBinding
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.model.Juice
import com.maestroinfotech.juiceapp.vendor.home.model.JuiceRegisterResponse
import com.squareup.picasso.Picasso

class JuiceAdapter constructor(val juices:List<JuiceRegisterResponse>) : RecyclerView.Adapter<JuiceAdapter.MyViewHolder>() {
    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {

        val binding = ProductItemBinding.bind(view)
        fun bindata(juice: JuiceRegisterResponse) {

            binding.productTitle.setText(juice.juiceName)


            Picasso.get().load(juice.path+juice.image)
                .placeholder(R.drawable.mango)
                .into(binding.productImage)

            binding.productPrice.setText(juice.rsPerCup)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(ProductItemBinding.inflate(LayoutInflater.from(parent.context)).root)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.bindata(juices.get(position))
    }

    override fun getItemCount(): Int {
        return juices.size
    }
}