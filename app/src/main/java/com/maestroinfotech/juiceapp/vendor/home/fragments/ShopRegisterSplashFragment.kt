package com.maestroinfotech.juiceapp.vendor.home.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.FragmentShopRegisterSplashBinding
import com.maestroinfotech.juiceapp.user.home.fragments.VendorHomeFragment
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.vendor.home.VendorHomeActivity
import com.maestroinfotech.juiceapp.vendor.home.model.ShopRegisterResponse
import io.paperdb.Paper


class ShopRegisterSplashFragment : Fragment() {



    lateinit var binding:FragmentShopRegisterSplashBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentShopRegisterSplashBinding.inflate(layoutInflater)

        Paper.init(requireContext())
        val shopRegisterUserResponse = Paper.book().read<ShopRegisterResponse>(Constant.REGISTERED_SHOP)
        binding.nextButton.setOnClickListener {


            VendorHomeActivity.navigateTo(requireFragmentManager(),
                AddFruitJuiceFragment())

        }

        if (shopRegisterUserResponse!=null){

            binding.shopNameText.text = "Shop name "+shopRegisterUserResponse.shopName

        }






        return binding.root
    }

}