package com.maestroinfotech.juiceapp.vendor.home.model


import com.google.gson.annotations.SerializedName

data class JuiceRegisterResponse(
    @SerializedName("add_description")
    val addDescription: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("juice_name")
    val juiceName: String?,
    @SerializedName("result")
    val result: String?,
    @SerializedName("rs_per_cup")
    val rsPerCup: String?,
    @SerializedName("strtotime")
    val strtotime: String?,
    @SerializedName("vendor")
    val vendor: String?,
    @SerializedName("path")
    val path: String?


)