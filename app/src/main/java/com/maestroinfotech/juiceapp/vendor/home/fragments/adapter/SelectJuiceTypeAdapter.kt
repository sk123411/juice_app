package com.maestroinfotech.juiceapp.vendor.home.fragments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestroinfotech.juiceapp.databinding.LayoutNewOnlineShopItemBinding

class SelectJuiceTypeAdapter (val tempJuice: List<TempJuice>) :RecyclerView.Adapter<SelectJuiceTypeAdapter.MyViewHolder>() {
    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {

        val binding = LayoutNewOnlineShopItemBinding.bind(view)
        fun setData(tempJuice: TempJuice) {

            binding.juiceImage.setImageResource(tempJuice.image!!)




        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
       return MyViewHolder(LayoutNewOnlineShopItemBinding.inflate(
           LayoutInflater.from(parent.context)
       ).root)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.setData(tempJuice.get(position))


    }





    override fun getItemCount(): Int {
        return tempJuice.size
    }
}