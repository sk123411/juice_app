package com.maestroinfotech.juiceapp.vendor.home.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.esotericsoftware.minlog.Log
import com.github.dhaval2404.imagepicker.ImagePicker
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.FragmentAddFruitJuiceBinding
import com.maestroinfotech.juiceapp.databinding.LayoutAddBowlItemBinding
import com.maestroinfotech.juiceapp.user.home.fragments.VendorHomeFragment
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.UserFragmentAdapterEventListener
import com.maestroinfotech.juiceapp.vendor.home.VendorHomeActivity
import com.maestroinfotech.juiceapp.vendor.home.model.JuiceRegisterResponse
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AddFruitJuiceFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddFruitJuiceFragment : Fragment() {

    lateinit var binding:LayoutAddBowlItemBinding
    var file:File?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        binding = LayoutAddBowlItemBinding.inflate(layoutInflater)
        binding.juiceProductImage.setImageResource(R.drawable.camera)
        binding.submitButton.setOnClickListener {

            if (binding.juiceName.text.toString().isEmpty()){
                Constant.showToast(requireContext(),"Please enter juice name")
                return@setOnClickListener
            }
            if (binding.juiceDescription.text.toString().isEmpty()){
                Constant.showToast(requireContext(),"Please enter juice description")
                return@setOnClickListener
            }


            if (binding.juicePerCup.text.toString().isEmpty()){
                Constant.showToast(requireContext(),"Please enter price of juice per cup")
                return@setOnClickListener
            }

            if (file!=null){
                createJuice(file)

            }else {
                Constant.showToast(requireContext(),"Please check product image")

            }

        }



        binding.nextButton.setOnClickListener {

            VendorHomeActivity.navigateTo(requireFragmentManager(),
            VendorHomeFragment())




        }


        binding.juiceProductImage.setOnClickListener {

            ImagePicker.with(requireActivity())
                .galleryOnly().maxResultSize(1024,1024)
                .crop()
                .start()

        }



        VendorHomeActivity.userFragmentAdapterEventListener = object :
            UserFragmentAdapterEventListener {
            override fun navigateToVendorDetail() {

            }

            override fun getPickedImageProduct(uri: Uri) {

                android.util.Log.d("TESTTTTTTTTt", "::"+uri)


                binding.juiceProductImage.setImageURI(uri)
                file = File(uri.path)


            }

        }





        return binding.root
    }

    private fun createJuice(file: File?) {
        Constant.showDialog(requireActivity(),"Adding product..")

        AndroidNetworking.upload("https://ruparnatechnology.com/juice/appservice/process.php?action=add_new_item")
            .addMultipartFile("image", file)
            .addMultipartParameter("user_id", Constant.getUserData(requireContext()).id)
            .addMultipartParameter("juice_name", binding.juiceName.text.toString())
            .addMultipartParameter("add_description", binding.juiceDescription.text.toString())
            .addMultipartParameter("rs_per_cup", binding.juicePerCup.text.toString())
            .build().getAsObject(JuiceRegisterResponse::class.java, object:ParsedRequestListener<JuiceRegisterResponse>{
                override fun onResponse(response: JuiceRegisterResponse?) {

                    if (response!!.result!!.contains("Successfull")){

                        Constant.dismiss()
                        Constant.showToast(requireContext(),"Product added successfully")
                        requireActivity().supportFragmentManager.popBackStack()




                    }


                }

                override fun onError(anError: ANError?) {


                }


            })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        android.util.Log.d("CALLEDDDDDDDDDDD", "result")

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val uri: Uri = data?.data!!

            file = File(uri.path)
            binding.juiceProductImage.setImageURI(uri)


        } else if (resultCode == ImagePicker.RESULT_ERROR) {

            Constant.showToast(requireContext(),ImagePicker.getError(data))

        } else {

            Constant.showToast(requireContext(),"Image pick cancelled")

        }



    }
}