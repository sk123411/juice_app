package com.maestroinfotech.juiceapp.vendor.home

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.navigation.NavigationView
import com.maestroinfotech.juiceapp.R
import com.maestroinfotech.juiceapp.databinding.ActivityVendorHomeBinding
import com.maestroinfotech.juiceapp.login.fragments.activity.LoginActivity
import com.maestroinfotech.juiceapp.user.home.fragments.VendorHomeFragment
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.UserFragmentAdapterEventListener
import com.maestroinfotech.juiceapp.vendor.home.fragments.ShopRegistrationFragment
import io.paperdb.Paper


class VendorHomeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVendorHomeBinding.inflate(layoutInflater)
        setContentView(binding!!.root)


        setSupportActionBar(binding!!.appBarMain.toolbarMain.toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar!!.setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24)
        actionbar!!.setDisplayHomeAsUpEnabled(true)



        binding!!.appBarMain.toolbarMain.toolbar.setNavigationOnClickListener {

            binding!!.drawerLayout.openDrawer(GravityCompat.START)

        }

        binding!!.navView.setNavigationItemSelectedListener(
            object : NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {

                when(item.itemId){


                    R.id.nav_edit_product -> {

                        binding!!.drawerLayout.closeDrawers()


                    }


                    R.id.nav_total_revenue -> {

                        binding!!.drawerLayout.closeDrawers()


                    }
                    R.id.nav_register_shop -> {

                        VendorHomeActivity.navigateTo(supportFragmentManager,
                        ShopRegistrationFragment())
                        binding!!.drawerLayout.closeDrawers()


                    }

                    R.id.nav_feedback -> {

                        binding!!.drawerLayout.closeDrawers()


                    }


                    R.id.nav_changePasword -> {

                        binding!!.drawerLayout.closeDrawers()


                    }


                    R.id.nav_offers -> {

                        binding!!.drawerLayout.closeDrawers()


                    }


                    R.id.nav_logout -> {

                        Paper.init(applicationContext)
                        Paper.book().delete(Constant.userDetails)


                        startActivity(Intent(applicationContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))


                        binding!!.drawerLayout.closeDrawers()


                    }


                    else -> {
                        binding!!.drawerLayout.closeDrawers()

                    }

                }

                     return false
            }

        })




        val fragmentManager = supportFragmentManager.beginTransaction()
            .add(R.id.containerMain, ShopRegistrationFragment()).commit()

    }


    companion object {
        var binding: ActivityVendorHomeBinding?=null

        fun navigateTo(supportFragmentManager: FragmentManager, fragment: Fragment) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.containerMain, fragment).addToBackStack(null).commit()
        }

        var userFragmentAdapterEventListener:UserFragmentAdapterEventListener?=null
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        VendorHomeActivity.userFragmentAdapterEventListener!!.getPickedImageProduct(
            data!!.data!!)







    }
}