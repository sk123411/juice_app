package com.maestroinfotech.juiceapp.vendor.home.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.maestroinfotech.juiceapp.databinding.LayoutShopRegistrationBinding
import com.maestroinfotech.juiceapp.user.home.fragments.VendorHomeFragment
import com.maestroinfotech.juiceapp.user.home.fragments.home.adapter.helpers.Constant
import com.maestroinfotech.juiceapp.vendor.home.VendorHomeActivity
import com.maestroinfotech.juiceapp.vendor.home.model.ShopRegisterResponse
import io.paperdb.Paper


class ShopRegistrationFragment : Fragment() {


    lateinit var binding:LayoutShopRegistrationBinding
    var progressBar:ProgressDialog? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        progressBar = ProgressDialog(requireActivity())
        progressBar!!.setMessage("Please wait...")

       VendorHomeActivity.binding!!.appBarMain.appBarLayout.visibility = View.GONE

        binding = LayoutShopRegistrationBinding.inflate(layoutInflater)


        binding.registerButton.setOnClickListener {



            if (binding.editRegistrationBbmp.text.toString().isEmpty()){

                Toast.makeText(context, "Please enter shop registration number", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }

            if (binding.editOwnerName.text.toString().isEmpty()){

                Toast.makeText(context, "Please enter owner name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }

            if (binding.editAddress.text.toString().isEmpty()){

                Toast.makeText(context, "Please enter address", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }

            if (binding.editShopName.text.toString().isEmpty()){

                Toast.makeText(context, "Please enter shop name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }

            if (binding.editAreaPincode.text.toString().isEmpty()){

                Toast.makeText(context, "Please enter pincode", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }

            registerShop()


        }





        return binding.root
    }

    private fun registerShop() {
        progressBar!!.show()

        AndroidNetworking.post("https://ruparnatechnology.com/juice/appservice/process.php?action=shop_registration")
            .addBodyParameter("shop_name", binding.editShopName.text.toString())
            .addBodyParameter("shop_registration_bbmp", binding.editRegistrationBbmp.text.toString())
            .addBodyParameter("owner_name", binding.editOwnerName.text.toString())
            .addBodyParameter("area_pincode", binding.editAreaPincode.text.toString())
            .addBodyParameter("address", binding.editAddress.text.toString())
            .build().getAsObject(ShopRegisterResponse::class.java, object:ParsedRequestListener<ShopRegisterResponse>{
                override fun onResponse(response: ShopRegisterResponse?) {

                    if (response!!.result!!.contains("Successfull")){

                        progressBar!!.dismiss()

                        Paper.init(requireContext())
                        Paper.book().write(Constant.REGISTERED_SHOP, response!!)
                       VendorHomeActivity.navigateTo(
                         requireActivity().supportFragmentManager,
                         ShopRegisterSplashFragment()
                     )


                    }

                }

                override fun onError(anError: ANError?) {
                    progressBar!!.dismiss()

                }

            })


    }


}